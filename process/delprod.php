<html>
    <head>
        <title>Admin</title>
        <meta charset="UTF-8">
        <meta http-equiv="Refresh" content="6;url=../configAdmin.php">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/media.css">
        <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/logo.ico" />
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/autohidingnavbar.min.js"></script>
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
        <?php
        session_start();
        include '../library/configServer.php';
        include '../library/consulSQL.php';
        $codeProd= $_POST['prod-code'];

        $cons=  ejecutarSQL::consultar("select * from producto where CodigoProd='$codeProd'");
        $totalproductos = mysqli_num_rows($cons);
        $tmp=  mysqli_fetch_array($cons);
        $imagen=$tmp['Imagen'];
        if($totalproductos>0){
           if(consultasSQL::DeleteSQL('producto', "CodigoProd='".$codeProd."'")){
               $carpeta='../assets/img-products/';
               $directorio=$carpeta.$imagen;
               chmod($directorio, 0777);
               unlink($directorio);
               echo '
                 <div class="alert alert-success alert-dismissable">
                         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                         <p class="lead text-center">El producto se elimino de la tienda con éxito</p>
                         <p class="lead text-cente">
                             La pagina se redireccionara automaticamente. Si no es asi haga click en el siguiente boton.<br>
                             <a href="../configAdmin.php" class="btn btn-primary btn-lg">Volver a administración</a>
                         </p>
                 </div>';

           }else{
             echo '
               <div class="alert alert-warning alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <p class="lead text-center">Ha ocurrido un error.<br>Por favor intente nuevamente</p>
                       <p class="lead text-cente">
                           La pagina se redireccionara automaticamente. Si no es asi haga click en el siguiente boton.<br>
                           <a href="../configAdmin.php" class="btn btn-primary btn-lg">Volver a administración</a>
                       </p>
               </div>';

           }
        }else{
          echo '
            <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p class="lead text-center">El código del producto no existe</p>
                    <p class="lead text-cente">
                        La pagina se redireccionara automaticamente. Si no es asi haga click en el siguiente boton.<br>
                        <a href="../configAdmin.php" class="btn btn-primary btn-lg">Volver a administración</a>
                    </p>
            </div>';

        }  ?>
        </div></div></div>
    </section>
</body>
</html>
