<html>
    <head>
        <title>Pedido</title>
        <meta charset="UTF-8">
        <meta http-equiv="Refresh" content="6;url=../configAdmin.php">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/media.css">
        <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/logo.ico" />
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/autohidingnavbar.min.js"></script>
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
        <?php
session_start();
include '../library/configServer.php';
include '../library/consulSQL.php';
$num=$_POST['clien-number'];
if($num=='notlog'){
   $nameClien=$_POST['clien-name'];
   $passClien=  md5($_POST['clien-pass']);
}
if($num=='log'){
   $nameClien=$_POST['clien-name'];
   $passClien=$_POST['clien-pass'];
}

$verdata=  ejecutarSQL::consultar("select * from cliente where Clave='".$passClien."' and Nombre='".$nameClien."'");
$num=  mysqli_num_rows($verdata);
if($num>0){
  if($_SESSION['sumaTotal']>0){


  $data= mysqli_fetch_array($verdata);
  $nitC=$data['Nombre'];
  $StatusV="Pendiente";

  /*Insertando datos en tabla venta*/
  $adm=ejecutarSQL::consultar("select Nombre from producto where CodigoProd='".$_SESSION['producto'][0]."'");
  while($fil = mysqli_fetch_array($adm)) {
      consultasSQL::InsertSQL("venta", "Fecha, Nombre, Descuento, TotalPagar, Estado,NombreA", "'".date('Y-m-d')."','".$nitC."','0','".$_SESSION['sumaTotal']."','".$StatusV."','$fil[Nombre]'");
  }
  /*recuperando el número del pedido actual*/
  $verId=ejecutarSQL::consultar("select * from venta where Nombre='$nitC' order by NumPedido desc limit 1");
  while($fila=mysqli_fetch_array($verId)){
     $Numpedido=$fila['NumPedido'];
  }

  /*Insertando datos en detalle de la venta*/
  for($i = 0;$i< $_SESSION['contador'];$i++){
      consultasSQL::InsertSQL("detalle", "NumPedido, CodigoProd, CantidadProductos", "'$Numpedido', '".$_SESSION['producto'][$i]."', '1'");

      /*Restando un stock a cada producto seleccionado en el carrito*/
    $prodStock=ejecutarSQL::consultar("select * from producto where CodigoProd='".$_SESSION['producto'][$i]."'");
    while($fila = mysqli_fetch_array($prodStock)) {
        $existencias = $fila['Stock'];
        consultasSQL::UpdateSQL("producto", "Stock=('$existencias'-1)", "CodigoProd='".$_SESSION['producto'][$i]."'");
    }
  }

    /*Vaciando el carrito*/
    unset($_SESSION['producto']);
    unset($_SESSION['contador']);


    /*correo*/
                 $nombrec='gerardo';
                 $apellidoc='Aguilar';
                 $usuarioc='gerardpike';
                 $passc='gerardpike';
                 include_once('../phpmailer/class.phpmailer.php');
                 include_once('../phpmailer/class.smtp.php');
                 $mail = new PHPMailer();
                 $mail->IsSMTP();
                 $mail->SMTPAuth = true;
                 $mail->SMTPSecure = "ssl";
                 $mail->Host = "smtp.gmail.com";
                 $mail->Port = 465;

                 $mail->Username = 'geekpsh@gmail.com';
                 $mail->Password = 'Mynewlife';
                 $mail->AddAddress('p03349@hotmail.com');
                 $mail->Subject = "Registro exitoso!!!";
                 $mail->Body = $nombrec;
                 $mail->MsgHTML("<center><h1>Bienvenido ".$nombrec." ".$apellidoc."</h1></center> <br><h4> Usted puede acceder a nuestra pagina con: <br> Usuario: " . $usuarioc . "<br> Contrase&ntilde;a: " . $passc . "<br>Tipo de Usuario: Cliente "
                         . "<br>Inicie sesion en: www.artemixteca.com </h4>");
?>
<div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
         <p class="lead text-center">Exito.<br>Su pedido se ha realizado exitosamente</p>
          <p class="lead text-cente">
              La pagina se redireccionara automaticamente. Si no es asi haga click en el siguiente boton.<br>
              <a href="../configAdmin.php" class="btn btn-primary btn-lg">Volver a Inicio</a>
          </p>
  </div>
<?php
  }else{
    ?>
    <div class="alert alert-warning alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <p class="lead text-center"><br>No has seleccionado ningún producto, revisa el carrito de compras</p>
              <p class="lead text-cente">
                  La pagina se redireccionara automaticamente. Si no es asi haga click en el siguiente boton.<br>
                  <a href="../configAdmin.php" class="btn btn-primary btn-lg">Volver a Inicio</a>
              </p>
      </div>
    <?php
  }

}else{
  ?>
  <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
           <p class="lead text-center">Error<br>El nombre o contraseña invalidos</p>
            <p class="lead text-cente">
                La pagina se redireccionara automaticamente. Si no es asi haga click en el siguiente boton.<br>
                <a href="../configAdmin.php" class="btn btn-primary btn-lg">Volver a Inicio</a>
            </p>
    </div>
  <?php
}?>
</div></div></div>
</section>
</body>
</html>
