
<?php
include 'library/configServer.php';
include 'library/consulSQL.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Productos</title>

    <?php include 'inc/link.php'; ?>
</head>
<body id="container-page-product" onload="viewData()">
    <?php include './inc/navbar.php'; ?>
    <section id="infoproduct">
        <div class="container">
            <div class="row">
                <div class="page-header">
                    <h1>Tienda <small class="tittles-pages-logo">Arte Mixteca en Linea</small></h1>
                </div>
                <?php
                    $CodigoProducto=$_GET['CodigoProd'];
                    $productoinfo=  ejecutarSQL::consultar("select * from producto where CodigoProd='".$CodigoProducto."'");
                    while($fila=mysqli_fetch_array($productoinfo)){
                        echo '
                            <div class="col-xs-12 col-sm-12">
                                <h3 class="text-center">DETALLES DE PRODUCTO</h3>
                                </div>
                            <div class="col-xs-12 col-sm-4">

                                <br><br>
                                <h4><strong>Nombre: </strong>'.$fila['NombreProd'].'</h4>
                                <h4><strong>Modelo: </strong>'.$fila['Modelo'].'</h4>
                                <h4><strong>Marca: </strong>'.$fila['Marca'].'</h4>
                                <h4><strong>Precio: </strong>$'.$fila['Precio'].'</h4>';
                                 if(isset($_SESSION['nombreAdmin'])){}else{
                                echo '<h4><strong>Vendedor: </strong>'.$fila['Nombre'].'</h4>';
                               }
                              echo '
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <br><br>
                                <img class="img-responsive" src="assets/img-products/'.$fila['Imagen'].'">
                            </div>
                            <br>
                            <br><br>
                            <br><br>
                            <br><br>
                            <br><br>';
                             if(isset($_SESSION['nombreAdmin'])){

                               echo '<div class="form-group">
                               <br><br>
                               <br><br>
                               <br><br>
                               <a href="configAdmin.php" class="col-xs-12 col-sm-3 btn btn-lg btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;&nbsp;Regresar a la Administracion</a>
                               </div>
                               ';
                             }else{
                            echo '<div class="form-group">
                            <a href="product.php" class="col-xs-12 col-sm-3 btn btn-lg btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;&nbsp;Regresar a la tienda</a>
                            </div>
                              <br><br>
                                <div class="form-group">
                                <button value="'.$fila['CodigoProd'].'" class="col-xs-12 col-sm-3 btn btn-lg btn-success botonCarrito"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;&nbsp; Añadir al carrito</button>
                                </div>
                                <br><br>
                                <div class="form-group">
                                <button class="col-xs-12 col-sm-3 btn btn-lg btn-default" data-toggle="modal" data-target="#vendedor" class="btn btn-danger"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp; Ver vendedor</button>
                                </div>
                            </div>

                            <!-- Modal delete -->

                            		<div class="modal fade" id="vendedor" tabindex="-1" role="dialog" aria-labelledby="vendedorLabel" aria-hidden="true">
                            			<div class="modal-dialog">
                            				<div class="modal-content">

                            					<div class="modal-header">
                            						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            						<h4 class="modal-title text-center" id="vendedorLabel">Datos del vendedor</h4>
                            					</div>
                                     <form>
                            					<div class="modal-body">
                                      ';
                                      $productoinfo=  ejecutarSQL::consultar("select * from administrador where Nombre='".$fila['Nombre']."'");
                                      while($fila=mysqli_fetch_array($productoinfo)){
                                          echo '


                                                  <h4><strong><i class="glyphicon glyphicon-user"></i>&nbsp;Usuario: </strong>'.$fila['Nombre'].'</h4>
                                                  <h4><strong><i class="glyphicon glyphicon-user"></i>&nbsp;Nombre Completo: </strong>'.$fila['NombreCompleto'].' '.$fila['Apellido'].'</h4>
                                                  <h4><strong><i class="glyphicon glyphicon-home"></i>&nbsp;Direccion: </strong>'.$fila['Direccion'].'</h4>

                                                  <h4><strong><i class="glyphicon glyphicon-phone-alt"></i>&nbsp;Telefono: </strong>'.$fila['Telefono'].'</h4>
                                                  <h4><strong><i class="glyphicon glyphicon-envelope"></i>&nbsp;Email: </strong>'.$fila['Email'].'</h4>

                                      ';
                                    }
                                    echo '
                            					</div>

                            					<div class="modal-footer">
                            						<button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
                            					</div>
                                   </form>
                            				</div>
                            			</div>
                            		</div>

                        ';
                    }}
                ?>

            </div>
            <div class="container">

            <h2>Envia un comentario</h2>
                <div class="panel panel-default">
                 <div class="panel-body">
                   <form method="post" id="comentar" action="">

<input type="hidden" id="codigo" name="codigo" value="<?php echo $CodigoProducto; ?>">
<?php if(isset($_SESSION['nombreAdmin'])){
?>
<input type="hidden" id="name1" name="name1" value="<?php echo $_SESSION['nombreAdmin']; ?>">
<?php } ?>
<?php if(isset($_SESSION['nombreUser'])){
?>
  <label id="name">Nombre:<?php echo $_SESSION['nombreUser']; ?></label>
  <input type="hidden" id="name" name="name" value="<?php echo $_SESSION['nombreUser']; ?>">

  <br>
  Comentario:<br/>
  <textarea rows="4" class="col-xs-12 col-sm-12" id="comment" cols="50"></textarea>
  <br/><br/>
  <button type="submit"  id="enviar-btn" class="btn btn-success col-xs-4  col-sm-2"></i>Enviar</button>
  <br>
<?php
}else{
  echo "Por favor inicie sesion para poder hacer comentarios";
}?>

        </form>
                </div>
                </div>
            <h2>Comentarios</h2>
           <div id="newmessage"></div>

          </div>

        </div>
    </section>
    <?php include './inc/footer.php'; ?>
</body>
</html>

<script >

$(document).ready(function() {
    $("#enviar-btn").click(function() {
        var name = $("input#name").val();
        var comment = $("textarea#comment").val();
        var codigo = $("input#codigo").val();
        if (name == '') {
            alert('Debe añadir su nombre.');
            return false;
        }
        if (comment == '') {
            alert('Debe añadir un comentario.');
            return false;
        }
        var dataString = 'name=' + name + '&comment=' + comment+ '&codigo=' + codigo;
        $.ajax({
                type: "POST",
                url: "addcomment.php",
                data: dataString,
                success: function(data) {
                viewData1();
                }
        });
        return false;
    });
});
</script>


<script>
function viewData(){
  var codigo = $("input#codigo").val();
  var name = $("input#name1").val();
  var dataString = 'name=' + name+'&codigo=' + codigo;
$.ajax({
  type: "POST",
  data: dataString,
  url:"comentario.php",
  success: function(data){
    $('#newmessage').html(data);
  }
})
}
</script>
<script>
function viewData1(){
  var codigo = $("input#codigo").val();
  var name = $("input#name1").val();
  var dataString = 'name=' + name+'&codigo=' + codigo;
$.ajax({
  type: "POST",
  data: dataString,
  url:"comentario1.php",
  success: function(data){
    $('#newmessage').html(data);
  }
})
}
</script>
