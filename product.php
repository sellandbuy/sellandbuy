<?php
include './library/configServer.php';
include './library/consulSQL.php';
include './process/securityadmin.php';

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Productos</title>
    <?php include './inc/link.php'; ?>
    <script src="js/carrito.js"></script>
</head>
<body id="container-page-product">
    <?php include './inc/navbar.php'; ?>
    <section id="store">
       <br>
        <div class="container">
            <div class="page-header">
              <h1>Tienda <small class="tittles-pages-logo">ArteMixteca En Linea</small></h1>
            </div>
                <form method="post" onchange="return pagina1(1); " >
                  <div class="form-group row">
                  <div class="col-sm-4">
                      <select name="opcconsulta"  id="opcconsulta" class="form-control" >
                        <option value="todos">Todos los productos</option>
                        <?php
                          $categorias=  ejecutarSQL::consultar("select * from categoria");
                          while($cate=mysqli_fetch_array($categorias)){
                            ?>
                            <option value="<?php echo $cate['CodigoCat']?>"><?php echo $cate['Nombre']?></option>
                          <?php  }
                        ?>
                      </select>
                  </div>
                </div>
              </form>


                  <form method="post" onsubmit="return pagina(1); "  >
                     <div class="row">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <input type="text" required="" name="consu" id="consu" class="form-control" placeholder="¿Que quieres buscar?">
                            <span class="input-group-btn">
                              <button class="btn btn-primary"  type="submit"><span class="glyphicon glyphicon-search"></span></button>
                            </span>
                          </div>
                        </div>
                      </div>

                    </form>

            <br>
            <div class="row">
                <div class="col-xs-12">

                    <div id="myTabContent" >

                        </div>

                    </div>
                </div>
  </div>
    </section>
    <?php include './inc/footer.php'; ?>
<script>
    function viewData(){
    var consu=document.getElementById('consu').value;
    $.ajax({
    type:"POST",
    url:"consulta.php",
    data:"consu="+consu,
    success: function(data){
      $('#myTabContent').html(data);
    }
    });
    return false;
    }
  </script>
  <script>
    function pagina1(page){
      var consu=document.getElementById('opcconsulta').value;
      var parametros = {"action":"ajax","page":page,"consu":consu};
      $("#loader").fadeIn('slow');
      $.ajax({
        url:'opcconsulta.php',
        type:"POST",
        data: parametros,
        success:function(data){
          $("#myTabContent").html(data).fadeIn('slow');
          $("#loader").html("");
        }
      });
      return false;
    }
  </script>
  <script>
    function pagina(page){
      var consu=document.getElementById('consu').value;
      var parametros = {"action":"ajax","page":page,"consu":consu};
      $("#loader").fadeIn('slow');
      $.ajax({
        url:'consulta.php',
        type:"POST",
        data: parametros,
        success:function(data){
          $("#myTabContent").html(data).fadeIn('slow');
          $("#loader").html("");
        }
      });
      return false;
    }
  </script>

<script>
  $("#fconsu").on("change",function(){
    load(1);
  });

  function load(page){
    var consu=document.getElementById('opcconsulta').value;
    var parametros = {"action":"ajax","page":page,"consu":consu};

    $("#loader").fadeIn('slow');
    $.ajax({
      url:'opcconsulta.php',
      type: "POST",
      data: parametros,
      success:function(data){
        $("#myTabContent").html(data).fadeIn('slow');
        $("#loader").html("");
      }
    })
  }
</script>
<script>
  $(document).ready(function(){
    load(1);
  });

  function load(page){
    var parametros = {"action":"ajax","page":page};
    $("#loader").fadeIn('slow');
    $.ajax({
      url:'producto_ajax.php',
      data: parametros,
      success:function(data){
        $("#myTabContent").html(data).fadeIn('slow');
        $("#loader").html("");
      }
    })
  }
  </script>
</body>
</html>
