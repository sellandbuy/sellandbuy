<?php
    include './process/securityadmin.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Registro</title>
    <?php include './inc/link.php'; ?>
    <script src="js/carrito.js"></script>
</head>
<body id="container-page-registration">
    <?php include './inc/navbar.php'; ?>
    <section id="form-registration">
        <div class="container">
            <div class="row">
                <div class="page-header text-center">
                  <h1>Registro de usuarios <small class="tittles-pages-logo">ArteMixteca En Linea</small></h1>
                </div>
                <div class="col-xs-12 col-md-offset-3 col-sm-6 text-center">
                   <br><br>
                    <div id="container-form">
                       <p style="color:black;" class="text-center">Debera de llenar todos los campos para registrarse</p>


                       <form method="POST" id="registrocli" data-form="save">

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    					<input class="mdl-textfield__input" type="text" required name="clien-fullname" data-toggle="tooltip" data-placement="top" title="Ingrese sus nombres.(solamente letras)" pattern="[a-zA-Z ]{1,50}" maxlength="50">
                    					<label class="mdl-textfield__label" ><i class="glyphicon glyphicon-user"></i>&nbsp;Nombres</label>
                    				</div>
                            <br>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                              <input class="mdl-textfield__input" type="text" required name="clien-lastname" data-toggle="tooltip" data-placement="top" title="Ingrese sus apellido(solamente letras)" pattern="[a-zA-Z ]{1,50}" maxlength="50">
                              <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-user"></i>&nbsp;Apellidos</label>
                            </div>
                            <br>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                              <input class="mdl-textfield__input" type="text" required name="clien-name" id="clien-name">
                              <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-user"></i>&nbsp;Nombre de usuario</label>
                            </div>
                            <br>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                              <input class="mdl-textfield__input" type="password"   name="clien-pass" data-toggle="tooltip" data-placement="top" title="Defina una contraseña para iniciar sesión">
                              <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-lock"></i>&nbsp;Contraseña</label>
                            </div>
                            <br>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                              <input class="mdl-textfield__input" type="text" required name="clien-dir" data-toggle="tooltip" data-placement="top" title="Ingrese la direción en la reside actualmente" maxlength="100">
                              <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-home"></i>&nbsp;Direccion</label>
                            </div>
                            <br>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                              <input class="mdl-textfield__input" type="tel" required name="clien-phone" maxlength="11" pattern="[0-9]{8,11}" data-toggle="tooltip" data-placement="top" title="Ingrese su número telefónico. Mínimo 8 digitos máximo 11">
                              <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-phone-alt"></i>&nbsp;Numero Telefónico</label>
                            </div>
                            <br>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                              <input class="mdl-textfield__input" type="email" required name="clien-email" data-toggle="tooltip" data-placement="top" title="Ingrese la dirección de su Email" maxlength="50">
                              <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-envelope"></i>&nbsp;Email</label>
                            </div>
                            <label for="">!Selecciona el tipo de cuenta que deseas crear¡</label>
                            <div class="form-group">
                              <div class="btn-group" data-toggle="buttons">
                                      <label class="btn btn-primary active">
                                        <input type="radio" name="reguser"  checked id="reguser1" value="vendedor"> Vendedor
                                      </label>
                                      <label class="btn btn-success">
                                        <input type="radio" name="reguser" id="reguser2" value="comprador"> Comprador
                                      </label>
                            </div>
                          </div>
                          <span><input type="checkbox" name="terminos" id="terminos1" value="si"><href data-toggle="modal" class="btn btn-sm btn-success" data-target="#term">Terminos y condiciones</href>


                          <div class="modal fade" id="term" tabindex="-1" role="dialog" aria-labelledby="termLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">

                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title"  style="text-align:center;"  id="termLabel">Terminos y condiciones</h4>
                                  </div>
                                  <div class="col-sm-12 text-left">
                                    <h5>La plataforma ArteMixteca usa y protege la información que es proporcionada por sus usuarios al momento de utilizar su sitio web. Esta página está comprometida con la seguridad de los datos de sus usuarios.
 Cuando le pedimos llenar los campos de información personal con la cual usted pueda ser identificado, lo hacemos asegurando que sólo se empleará de acuerdo con los términos de este documento.
La información que es recogida Nuestro sitio web podrá recoger información personal por ejemplo: Nombre, información de contacto como su dirección de correo electrónica, teléfono, dirección. Así mismo cuando sea necesario podrá ser requerida información específica para procesar algún pedido o realizar una venta.
</h5><h5>
Toda la información que se ingresa es empleada con el fin de proporcionar el mejor servicio posible y así evitar ciertas inconformidades con el cliente o con el vendedor de un producto, particularmente para mantener un registro de usuarios, de pedidos en caso que aplique, y mejorar nuestros productos y servicios.
Usted al visitar la página web opcionalmente puede ingresar sus datos mientras no realice alguna operación sobre el sistema. En caso contrario es necesario agregar la información solicitada para poder exhibir sus artículos o para realizar la compra de un artículo.
SellandOnline está altamente comprometido para cumplir con el compromiso de mantener su información segura, en caso contrario se cargaran cargos al personal administrativo de la página por difusión de información no autorizada.
Uso de información personal.
Correo electrónico</h5>
                                  </div>
                                  <div class="text-left col-sm-12">
                                    <h5>
                                    Por este medio el vendedor y el comprador pueden comunicarse para afinar los detalles para la venta si en caso de que alguno de ellos no cuente con un número telefónico. Toda la información que se está manejando para el correo son las ventas, el vendedor recibirá un correo cada vez que un comprador adquiere sus productos.
Número telefónico.
El comprador puede ver algunos datos del vendedor, uno de esos datos es el número telefónico. Por este medio el comprador puede contactar directamente al vendedor del producto que quiera adquirir en la página.
Cuenta para vendedores.</h5>
<h5>1.	Utilizar la información de los clientes para poder realizar el proceso de la venta de productos y no para otros fines.</h5>
<h5>2.	El sospecho del uso inadecuado de información de los clientes será atendida provocando que la cuenta sea desactivada hasta que se compruebe lo contrario, y en caso de que esto sea verdad se cobrara una multa y la cuenta quedara desactivada así mismo ya no podrá exhibir los productos por este medio.
</h5>
<h5>Como vendedor puede exhibir sus productos con la condición de que pague una cuota de $1000.00 anual por el uso de los servicios que ofrece SellandOnline, en caso de que no se realice dicho pago los servicios que brinda la página web serán retiradas hasta que se realice el pago.
Cuenta de clientes.</h5>
<h5>1.- la información del vendedor que se proporciona es para poder dar seguimiento al proceso de la adquisición de productos que se desee.</h5>
<h5>2.-el mal uso de la información que se esté asignado, se levantara cargos por las autoridades correspondientes y así asegurar</h5>
<h5>3.- la cuenta quedara inhabilitada en caso de que se esté investigando un delito sobre el uso inadecuado de información, se habilitara hasta que se compruebe que es inocente.
</h5>

<h5>¿Qué INFORMACION RECOPILAMOS?
SE RECOPILAN DATOS EN FUNCION CONTIGO DEPENDIENDO DEL SERVICIO QUE ESTES USANDO.</h5>
<h5>-ACCIONES Y LA INFORMACION QUE PROPORCIONAS.</h5>
<h5>Recopilamos los datos que proporcionas cuando usas un servicio, por ejemplo como vendedor debes ingresar ciertos datos para poder vender tus productos, como cliente recopilamos ciertos datos para que puedas adquirir tus productos.
-Información sobre pagos.
Si usas nuestros servicios para vender productos artesanos, recopilamos los datos del vendedor, al usar este servicio tienes la obligación de pagar una monto anual para el mantenimiento del sistema y por el derecho para el uso de los servicios que se estén brindando.
¿Cómo se usa esta información?
Usamos toda esta información para ofrecer un mejor servicio a los vendedores y a sus clientes.</h5>


  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
                                  </div>
                                </form>
                                </div>
                              </div>
                            </div>


                          <br>
                          <br>
                            <p><button type="submit" class="btn col-md-offset-2 btn-success col-xs-6  col-sm-4"><i class="glyphicon glyphicon-plus"></i> Registrarse</button>
                              <button type="reset" class="btn btn-danger col-xs-6 col-sm-4"><i class="glyphicon glyphicon-trash"></i> Borrar</button>
                            </p>
                            <br>
                            <br>
                            <div class="ResForm" style="width: 100%; color: #fff; text-align: center; margin: 0;"></div>

                            <div  id="mensaje">  </div>
                          </div>
                        </form>

            </div>
            </div>
        </div>
    </section>
    <?php include './inc/footer.php'; ?>



</body>
</html>
<script>
$("#registrocli").on("submit", function(e){
  e.preventDefault();
  var formData = new FormData(document.getElementById("registrocli"));
  $.ajax({
    url: "process/regclien.php",
    type: "POST",
    dataType: "HTML",
    data: formData,
    cache: false,
    contentType: false,
    processData: false
  }).done(function(echo){
    $("#mensaje").html(echo);
  });
});
</script>
