<?php
    session_start();
    error_reporting(E_PARSE);
    if(!isset($_SESSION['contador'])){
        $_SESSION['contador'] = 0;
    }
?>
<section id="container-carrito-compras">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div id="carrito-compras-tienda"></div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <p class="text-center" style="font-size: 80px;">
                    <i class="glyphicon glyphicon-shopping-cart"></i>
                </p>
                <p class="text-center">

                    <a href="pedido.php" class="btn btn-success btn-block"><i class="glyphicon glyphicon-usd"></i>   Confirmar pedido</a>
                    <a href="process/vaciarcarrito.php" class="btn btn-danger btn-block"><i class="glyphicon glyphicon-trash"></i>   Vaciar carrito</a>
                </p>
            </div>
        </div>
    </div>
</section>
<nav id="navbar-auto-hidden">
        <div class="row hidden-xs">
            <div class="col-xs-4">
                <figure class="logo-navbar"></figure>
                <p class="text-navbar tittles-pages-logo">Tienda ArteMixteca</p>
            </div>
            <div class="col-xs-8">
                <div class="contenedor-tabla pull-right">
                    <div class="contenedor-tr">

                        <?php
                            if(!$_SESSION['nombreAdmin']==""){
                                echo '
                                    <a href="configAdmin.php" class="table-cell-td"><i class="glyphicon glyphicon-briefcase">&nbsp;&nbsp;</i>Administración</a>

                                    <a href="#" class="table-cell-td" data-toggle="modal" data-target=".modal-logout">
                                        <i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;'.$_SESSION['nombreAdmin'].'
                                    </a>
                                 ';
                            }else if(!$_SESSION['nombreUser']==""){
                                echo '
                                <a href="index.php" class="table-cell-td">  <i class="glyphicon glyphicon-home">&nbsp;&nbsp;</i>Inicio</a>
                                <a href="product.php" class="table-cell-td"><i class="glyphicon glyphicon-list-alt">&nbsp;&nbsp;</i>Productos</a>
                                    <a href="pedido.php" class="table-cell-td"><i class="glyphicon glyphicon-time">&nbsp;&nbsp;</i>Pedido</a>
                                    <a href="#" class="table-cell-td carrito-button-nav all-elements-tooltip" data-toggle="tooltip" data-placement="bottom" title="Ver carrito de compras">
                                        <i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                    <a href="#" class="table-cell-td" data-toggle="modal" data-target=".modal-logout">
                                        <i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;'.$_SESSION['nombreUser'].'
                                    </a>
                                 ';
                            }else{
                                echo '
                                <a href="index.php" class="table-cell-td">  <i class="glyphicon glyphicon-home">&nbsp;&nbsp;</i>Inicio</a>
                                <a href="product.php" class="table-cell-td"><i class="glyphicon glyphicon-list-alt">&nbsp;&nbsp;</i>Productos</a>
                                    <a href="registration.php" class="table-cell-td"><i class="glyphicon glyphicon-plus-sign">&nbsp;&nbsp;</i>Registro</a>
                                    <a href="#" class="table-cell-td carrito-button-nav all-elements-tooltip" data-toggle="tooltip" data-placement="bottom" title="Ver carrito de compras">
                                        <i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                    <a href="#" class="table-cell-td" data-toggle="modal" data-target=".modal-login">
                                        <i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Login
                                    </a>
                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLScahF3BnwKt7bvEn_muip9Hw_YRr6hM87cqYC5d159T1Cz93A/viewform?usp=sf_link" target="_blank" class="table-cell-td"><i class="glyphicon glyphicon-list-alt">&nbsp;&nbsp;</i></a>
                                 ';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row visible-xs"><!-- Mobile menu navbar -->
            <div class="col-xs-12">

                <?php
                if(!$_SESSION['nombreAdmin']==""){echo '
                  <button class="btn btn-default pull-left " id="btn-mobile-menu">
                      <i class="glyphicon "></i>
                  </button>
                  <a href="configAdmin.php" id="button-shopping-cart-xs" class="elements-nav-xs " data-toggle="tooltip" data-placement="bottom" >

                      <i class="glyphicon glyphicon-briefcase"></i>&nbsp;&nbsp;Administracion
                  </a>
                    <a href="#"  id="button-login-xs" class="elements-nav-xs" data-toggle="modal" data-target=".modal-logout">
                        <i class="glyphicon glyphicon-user"></i>&nbsp; '.$_SESSION['nombreAdmin'].'
                    </a>';
                }else if(!$_SESSION['nombreUser']==""){
                    echo '
                    <button class="btn btn-default pull-left button-mobile-menu" id="btn-mobile-menu">
                        <i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;Menú
                    </button>
                    <a href="#" id="button-shopping-cart-xs" class="elements-nav-xs all-elements-tooltip carrito-button-nav" data-toggle="tooltip" data-placement="bottom" title="Ver carrito de compras">
                        <i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-chevron-down"></i>
                    </a>
                    <a href="#"  id="button-login-xs" class="elements-nav-xs" data-toggle="modal" data-target=".modal-logout">
                        <i class="glyphicon glyphicon-user"></i>&nbsp; '.$_SESSION['nombreUser'].'
                    </a>';
                }else{
                    echo '
                    <button class="btn btn-default pull-left button-mobile-menu" id="btn-mobile-menu">
                        <i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;Menú
                    </button>
                    <a href="#" id="button-shopping-cart-xs" class="elements-nav-xs all-elements-tooltip carrito-button-nav" data-toggle="tooltip" data-placement="bottom" title="Ver carrito de compras">
                        <i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-chevron-down"></i>
                    </a>
                       <a href="#" data-toggle="modal" data-target=".modal-login" id="button-login-xs" class="elements-nav-xs">
                        <i class="glyphicon glyphicon-user"></i>&nbsp; Iniciar Sesión
                        </a>
                   ';
                }
                ?>
            </div>
        </div>
    </nav>
    <!-- Modal login -->
    <link rel="stylesheet" href="css/material.min.css">
    <script src="js/material.min.js"></script>

    <div class="modal fade modal-login" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm"  >
          <div class="modal-content" style="background-color:#f7f7f7;" id="modal-form-login">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove-sign"></i></button>
                  <h4 class="modal-title text-center text-primary" id="myModalLabel">Iniciar sesión en la Tienda</h4>
                </div>
            <form action="process/login.php" method="post" role="form" style="margin: 20px;" class="sellandbuy" data-form="login">
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
          					<input class="mdl-textfield__input" type="text" name="nombre-login">
          					<label class="mdl-textfield__label"><i class="glyphicon glyphicon-user"></i>&nbsp;Nombre de Usuario</label>
          				</div><br>
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
          					<input class="mdl-textfield__input" type="password" name="clave-login">
          					<label class="mdl-textfield__label"><i class="glyphicon glyphicon-lock"></i>&nbsp;Contraseña</label>
          				</div>
                  <p class="modal-title text-center text-primary">¿Cómo iniciaras sesión?</p>
                  <div class="radio">
                    <label>
                        <input type="radio"  name="optionsRadios" value="option1" checked>
                        Comprador
                    </label>
                 </div>
                 <div class="radio">
                    <label>
                        <input type="radio" name="optionsRadios" value="option2">
                         Vendedor
                    </label>
                 </div>
                  <div class="modal-footer " style="text-align:center;">
                    <button type="submit" class=" btn btn-success btn-sm"><i class="glyphicon glyphicon-ok">&nbsp;</i>Aceptar</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="glyphicon glyphicon-remove">&nbsp;</i>Cancelar</button>
                  </div>
                  <div class="ResFormL" style="width: 100%; text-align: center; margin: 0;"></div>
              </form>
          </div>
      </div>
    </div>
    <!-- Fin Modal login -->
    <div id="mobile-menu-list" class="hidden-sm hidden-md hidden-lg">
        <br>
        <h3 class="text-center tittles-pages-logo">ArteMixteca en Linea</h3>
        <button class="btn btn-default button-mobile-menu" id="button-close-mobile-menu">
        <i class="glyphicon glyphicon-remove"></i>
        </button>
        <br><br>
        <ul class="list-unstyled text-center">
            <li><a href="index.php"><i class="glyphicon glyphicon-home">&nbsp;&nbsp;</i>Inicio</a></li>
            <li><a href="product.php"><i class="glyphicon glyphicon-list-alt">&nbsp;&nbsp;</i>Productos</a></li>
            <?php
                if(!$_SESSION['nombreAdmin']==""){
                    echo '<li><a href="configAdmin.php"><i class="glyphicon glyphicon-briefcase">&nbsp;&nbsp;</i>Administración</a></li>';
                }elseif(!$_SESSION['nombreUser']==""){
                    echo '<li><a href="pedido.php"><i class="glyphicon glyphicon-time">&nbsp;&nbsp;</i>Pedido</a></li>';
                }else{
                    echo '<li><a href="registration.php"><i class="glyphicon glyphicon-plus-sign">&nbsp;&nbsp;</i>Registro</a></li>';
                }
            ?>
        </ul>
    </div>
    <!-- Modal carrito -->
    <div class="modal fade modal-carrito" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="padding: 20px;">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <br>
            <p class="text-center" style="font-size: 50px;">
                <i class="glyphicon glyphicon-shopping-cart"></i>
            </p>
            <p class="text-center">El producto se añadio al carrito</p>
            <p class="text-center"><button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Aceptar</button></p>
          </div>
      </div>
    </div>
    <!-- Fin Modal carrito -->

    <!-- Modal logout -->
    <div class="modal fade modal-logout" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="padding: 20px;">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <br>
            <p class="text-center">¿Estas seguro que quieres salir?</p>
            <p class="text-center" style="font-size: 50px;">
                <i class="glyphicon glyphicon-exclamation-sign"></i>
            </p>
            <p class="text-center">
                <a href="process/logout.php" class="btn btn-primary btn-sm">Aceptar</a>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
            </p>
          </div>
      </div>
    </div>
    <!-- Fin Modal logout -->
