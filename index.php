<?php
    include './process/securityadmin.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Inicio</title>
    <?php include './inc/link.php'; ?>
  <script src="js/carrito.js"></script>
</head>
<body id="container-page-index">
    <?php include './inc/navbar.php'; ?>

    <div class="jumbotron" id="jumbotron-index">
      <h1><span class="tittles-pages-logo">ArteMixteca</span> <small style="color: #fff;"> en Linea</small></h1>
      <p>
          Bienvenido a nuestra tienda en linea, aqui encontrara cafe.
      </p>
    </div>
    <section id="new-prod-index">
         <div class="container">
            <div class="page-header">
                <h1>Productos Nuevos</small></h1>
            </div>
            <div class="outer_div">

        		</div>
         </div>
    </section>
    <section id="reg-info-index">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-offset-3 col-sm-6 text-center">
                   <article>
                     <p class="text-center" style="font-size: 80px;">
                         <i class="glyphicon glyphicon-user"></i>
                     </p>
                          <p><a href="registration.php" class="btn btn-success btn-block">Registrarse</a></p>
                          <h5>Para poder comprar o vender productos usted debe registrarse en la tienda</h5>
                   </article>
                </div>

            </div>
        </div>
    </section>
    <section id="distribuidores-index">
        <div class="container">

          <!--  <div class="col-xs-12">
                <div class="page-header">
                  <h1><i class="glyphicon glyphicon-send"></i>&nbsp;&nbsp;Metodos de envio <small style="color: #333;"></small></h1>
                </div>
                <br><br>

            </div> -->
        </div>
    </section>
    <?php include './inc/footer.php'; ?>

</body>
</html>
<script>
$(document).ready(function(){
  load(1);
});

function load(page){
  var parametros = {"action":"ajax","page":page};
  $("#loader").fadeIn('slow');
  $.ajax({
    url:'producto_ajax.php',
    data: parametros,
    success:function(data){
      $(".outer_div").html(data).fadeIn('slow');
      $("#loader").html("");
    }
  })
}
</script>
