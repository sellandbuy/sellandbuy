<?php
    include './library/configServer.php';
    include './library/consulSQL.php';
    include './process/securityPanel.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Admin</title>
    <?php include './inc/link.php'; ?>
  <script type="text/javascript" src="js/admin.js"></script>
</head>
<body id="container-page-configAdmin" >
    <input type="hidden"  id="name-admin" value="<?php echo $_SESSION['nombreAdmin'] ?>">
    <?php include './inc/navbar.php'; ?>
    <section id="prove-product-cat-config">
        <div class="container">
            <div class="page-header">
              <h4>Panel de Administracion </h4>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#Productos" role="tab" data-toggle="tab">Productos</a></li>
            <?php if (isset($_SESSION['superadmin-name'])) {?>
              <li role="presentation"><a href="#Categorias" role="tab" data-toggle="tab">Categorías</a></li>
          <?php } ?>

              <li role="presentation"><a href="#Admins" role="tab" data-toggle="tab">Perfil</a></li>
              <li role="presentation"><a href="#Pedidos" role="tab" data-toggle="tab">Pedidos</a></li>
              <li role="presentation"><a href="#Reportes" role="tab" data-toggle="tab">Reportes PDF</a></li>
            </ul>
            <div class="tab-content">
                <!--==============================Panel productos===============================-->
                <div role="tabpanel" class="tab-pane fade in active" id="Productos">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <br><br>
                        <div id="add-product">

                            <h5 class="text-primary text-center"><small><i class="fa fa-plus"></i></small>&nbsp;&nbsp;Agregar un producto nuevo</h5>

                             <p class="text-center"><button data-toggle="modal" data-target="#addproducto" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Agregar</button></p>
                      				<div class="modal fade" id="addproducto" tabindex="-1" role="dialog" aria-labelledby="addproductoLabel" aria-hidden="true">
                      						<div class="modal-dialog">
                      							<div class="modal-content">

                      								<div class="modal-header">
                      									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      									<h4 class="modal-title"  style="text-align:center;"  id="addproductoLabel">Agregar Producto</h4>
                      								</div>
                      					  <form role="form" action="process/regproduct.php" method="post" enctype="multipart/form-data">
                      								<div class="modal-body text-center">

                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    					<input class="mdl-textfield__input" type="text" class="form-control"   required maxlength="30" name="prod-codigo">
                                    					<label class="mdl-textfield__label" >Codigo de producto</label>
                                    				</div>
                                            <br>
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    					<input class="mdl-textfield__input" type="text" class="form-control"   required maxlength="30" name="prod-name">
                                    					<label class="mdl-textfield__label" >Nombre de producto</label>
                                    				</div>
                                            <br>

                                          <div class="form-group  col-sm-offset-3 col-sm-6">
                                            <label>Categoría</label>
                                            <select class="form-control" name="prod-categoria">
                                                <?php
                                                    $categoriac=  ejecutarSQL::consultar("select * from categoria");
                                                    while($catec=mysqli_fetch_array($categoriac)){
                                                        echo '<option value="'.$catec['CodigoCat'].'">'.$catec['Nombre'].'</option>';
                                                    }
                                                ?>
                                            </select>
                                          </div>
                                          <br>
                                          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                            <input class="mdl-textfield__input"  type="text" class="form-control"   required maxlength="20" pattern="[0-9]{1,20}" name="prod-price">
                                            <label class="mdl-textfield__label" >Precio</label>
                                          </div>
                                          <br>
                                          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                            <input class="mdl-textfield__input" type="text" class="form-control"  required maxlength="30" name="prod-model" >
                                            <label class="mdl-textfield__label" >Modelo</label>
                                          </div>
                                          <br><div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                            <input class="mdl-textfield__input" type="text" class="form-control"   required maxlength="30" name="prod-marca" >
                                            <label class="mdl-textfield__label" >Marca</label>
                                          </div>
                                          <br><div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                            <input class="mdl-textfield__input" type="text" class="form-control"   required maxlength="20" pattern="[0-9]{1,20}" name="prod-stock" >
                                            <label class="mdl-textfield__label" >Unidades disponibles</label>
                                          </div>
                                          <br>

                                          <div class="form-group">
                                            <label>Imagen de producto</label>
                                            <input class="col-sm-offset-3" type="file" name="img">
                                            <p class="help-block">Formato de imagenes admitido png, jpg, gif, jpeg</p>
                                          </div>
                                            <input type="hidden"  name="admin-name" value="<?php echo $_SESSION['nombreAdmin'] ?>">
                                          <div id="res-form-add" style="width: 100%; text-align: center; margin: 0;"></div>

                      								</div>

                      								<div class="modal-footer">
                      									<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                      								<button type="submit" class="btn btn-success">Agregar a la tienda</button>
                      								</div>
                      							</form>
                      							</div>
                      						</div>
                      					</div>

                        </div>
                    </div>
                    <form method="post" onchange="return pagina1(1); " >
                      <div class="form-group row">
                      <div class="col-sm-4 col-xs-12">
                        <br><br>
                          <select name="opcconsulta"  id="opcconsulta" class="form-control" >
                            <option value="todos">Todos los productos</option>
                            <?php
                              $categorias=  ejecutarSQL::consultar("select * from categoria");
                              while($cate=mysqli_fetch_array($categorias)){
                                ?>
                                <option value="<?php echo $cate['CodigoCat']?>"><?php echo $cate['Nombre']?></option>
                              <?php  }
                            ?>
                          </select>
                      </div>
                    </div>
                  </form>


                      <form method="post" onsubmit="return pagina(1); "  >
                         <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="input-group">
                                <input type="text" required="" name="consu" id="consu" class="form-control" placeholder="¿Que quieres buscar?">
                                <span class="input-group-btn">
                                  <button class="btn btn-primary"  type="submit"><span class="glyphicon glyphicon-search"></span></button>
                                </span>
                              </div>
                            </div>
                          </div>
                        </form>

                    <div class=" col-xs-12">
                        <br><br>
                        <div class="outer_div">

                    		</div>
                </div>
                </div>
                </div>

                <!--==============================Panel Categorias===============================-->
                <div role="tabpanel" class="tab-pane fade" id="Categorias">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <br><br>
                            <div id="add-categori">
                                <h2 class="text-info text-center"><small><i class="fa fa-plus"></i></small>&nbsp;&nbsp;Agregar categoría</h2>
                                <form action="process/regcategori.php" method="post" role="form">
                                    <div class="form-group">
                                        <label>Código</label>
                                        <input class="form-control" type="text" name="categ-code" placeholder="Código de categoria" maxlength="9" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input class="form-control" type="text" name="categ-name" placeholder="Nombre de categoria" maxlength="30" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Descripción</label>
                                        <input class="form-control" type="text" name="categ-descrip" placeholder="Descripcióne de categoria" required="">
                                    </div>
                                    <p class="text-center"><button type="submit" class="btn btn-primary">Agregar categoría</button></p>
                                    <br>
                                    <div id="res-form-add-categori" style="width: 100%; text-align: center; margin: 0;"></div>
                                </form>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <br><br>
                            <div id="del-categori">
                                <h2 class="text-danger text-center"><small><i class="fa fa-trash-o"></i></small>&nbsp;&nbsp;Eliminar una categoría</h2>
                                <form action="process/delcategori.php" method="post" role="form">
                                    <div class="form-group">
                                        <label>Categorías</label>
                                        <select class="form-control" name="categ-code">
                                            <?php
                                                $categoriav=  ejecutarSQL::consultar("select * from categoria");
                                                while($categv=mysqli_fetch_array($categoriav)){
                                                    echo '<option value="'.$categv['CodigoCat'].'">'.$categv['CodigoCat'].' - '.$categv['Nombre'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <p class="text-center"><button type="submit" class="btn btn-danger">Eliminar categoría</button></p>
                                    <br>
                                    <div id="res-form-del-cat" style="width: 100%; text-align: center; margin: 0;"></div>
                                </form>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <br><br>
                            <div class="panel panel-info">
                                <div class="panel-heading text-center"><i class="fa fa-refresh fa-2x"></i><h3>Actualizar categoría</h3></div>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead class="">
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th class="text-center">Nombre</th>
                                                <th class="text-center">Descripción</th>
                                                <th class="text-center">Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                              $categorias=  ejecutarSQL::consultar("select * from categoria");
                                              $ui=1;
                                              while($cate=mysqli_fetch_array($categorias)){
                                                  echo '
                                                      <div id="update-category">
                                                        <form method="post" action="process/updateCategory.php" id="res-update-category-'.$ui.'">
                                                          <tr>
                                                              <td>
                                                                <input class="form-control" type="hidden" name="categ-code-old" maxlength="9" required="" value="'.$cate['CodigoCat'].'">
                                                                <input class="form-control" type="text" name="categ-code" maxlength="9" required="" value="'.$cate['CodigoCat'].'">
                                                              </td>
                                                              <td><input class="form-control" type="text" name="categ-name" maxlength="30" required="" value="'.$cate['Nombre'].'"></td>
                                                              <td><input class="form-control" type="text-area" name="categ-descrip" required="" value="'.$cate['Descripcion'].'"></td>
                                                              <td class="text-center">
                                                                  <button type="submit" class="btn btn-sm btn-primary button-UC" value="res-update-category-'.$ui.'">Actualizar</button>
                                                                  <div id="res-update-category-'.$ui.'" style="width: 100%; margin:0px; padding:0px;"></div>
                                                              </td>
                                                          </tr>
                                                        </form>
                                                      </div>
                                                      ';
                                                  $ui=$ui+1;
                                              }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                              </div>
                        </div>
                    </div>
                </div>
                <!--==============================Panel Admins===============================-->
                <div role="tabpanel" class="tab-pane fade" id="Admins">
                    <div class="row">
                      <div class="col-xs-12 col-sm-6 ">
                          <br><br>
                          <div id="add-admin" class="text-center">
                              <h2 class="text-info text-center"><small><i class="fa fa-plus"></i></small>&nbsp;&nbsp;Actualizar mi contraseña</h2>
                              <form id="actulizarcontra" method="post" >
                              <?php
                            echo '<input type="hidden"  name="nadmin" value="'.$_SESSION['nombreAdmin'].'">';
                            $nadmin=$_SESSION['nombreAdmin'];
                              $adminCon=  ejecutarSQL::consultar("select * from administrador where nombre='$nadmin'");
                              while($AdminD=mysqli_fetch_array($adminCon)){
                                  echo '
                                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" required name="clien-name" disabled="true" value="'.$AdminD['Nombre'].'" id="clien-name" data-toggle="tooltip" data-placement="top" title="Ingrese su nombre. Máximo 9 caracteres (solamente letras)" pattern="[a-zA-Z]{1,9}" maxlength="9">
                                    <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-user"></i>&nbsp;Nombre de usuario</label>
                                  </div>
                                  <br>
                                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="password"  required name="clien-pass1" data-toggle="tooltip" data-placement="top" title="Defina una contraseña para iniciar sesión">
                                    <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-lock"></i>&nbsp;Contraseña 1</label>
                                  </div>
                                  <br>
                                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="password"  required name="clien-pass2" data-toggle="tooltip" data-placement="top" title="Defina una contraseña para iniciar sesión">
                                    <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-lock"></i>&nbsp;Contraseña 2</label>
                                  </div>
                                  <br>
                                  <p class="text-center"><button type="submit" class="btn btn-primary">Actualizar Contraseña</button></p>
                                  <br>
                                  <div id="res-form-add-admin" style="width: 100%; text-align: center; margin: 0;"></div>
                                  <div id="mensaje1"></div>
                              ';  } ?>
                              </form>
                          </div>
                      </div>
                        <div class="col-xs-12 col-sm-6 ">
                            <br><br>
                            <div id="add-admin" class="text-center">
                                <h2 class="text-info text-center"><small><i class="fa fa-plus"></i></small>&nbsp;&nbsp;Actualizar mis datos</h2>
                                <form id="actulizarcli" method="post" >
                                <?php
                                echo '<input type="hidden"  name="nadmin" value="'.$_SESSION['nombreAdmin'].'">';
                                $nadmin=$_SESSION['nombreAdmin'];
                                $adminCon=  ejecutarSQL::consultar("select * from administrador where nombre='$nadmin'");
                                while($AdminD=mysqli_fetch_array($adminCon)){
                                    echo '
                                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            					<input class="mdl-textfield__input" disabled="true" value="'.$AdminD['NombreCompleto'].'" type="text" required name="clien-fullname" data-toggle="tooltip" data-placement="top" title="Ingrese sus nombres.(solamente letras)" pattern="[a-zA-Z ]{1,50}" maxlength="50">
                            					<label class="mdl-textfield__label" ><i class="glyphicon glyphicon-user"></i>&nbsp;Nombres</label>
                            				</div>
                                    <br>
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                      <input class="mdl-textfield__input" disabled="true" value="'.$AdminD['Apellido'].'" type="text" required name="clien-lastname" data-toggle="tooltip" data-placement="top" title="Ingrese sus apellido(solamente letras)" pattern="[a-zA-Z ]{1,50}" maxlength="50">
                                      <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-user"></i>&nbsp;Apellidos</label>
                                    </div>
                                    <br>
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                      <input class="mdl-textfield__input" type="text" required value="'.$AdminD['Direccion'].'" name="clien-dir" data-toggle="tooltip" data-placement="top" title="Ingrese la direción en la reside actualmente" maxlength="100">
                                      <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-home"></i>&nbsp;Direccion</label>
                                    </div>
                                    <br>
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                      <input class="mdl-textfield__input" type="tel" required name="clien-phone" value="'.$AdminD['Telefono'].'" maxlength="11" pattern="[0-9]{8,11}" data-toggle="tooltip" data-placement="top" title="Ingrese su número telefónico. Mínimo 8 digitos máximo 11">
                                      <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-phone-alt"></i>&nbsp;Numero Telefónico</label>
                                    </div>
                                    <br>
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                      <input class="mdl-textfield__input" type="email" value="'.$AdminD['Email'].'" required name="clien-email" data-toggle="tooltip" data-placement="top" title="Ingrese la dirección de su Email" maxlength="50">
                                      <label class="mdl-textfield__label" ><i class="glyphicon glyphicon-envelope"></i>&nbsp;Email</label>
                                    </div>

                                    <p class="text-center"><button type="submit" class="btn btn-primary">Actualizar Datos</button></p>
                                    <br>
                                    <div id="res-form-add-admin" style="width: 100%; text-align: center; margin: 0;"></div>
                                    <div id="mensaje2"></div>
                                ';  } ?>
                                </form>
                            </div>
                        </div>
                        <div class="col-xs-12"></div>
                    </div>
                </div>
                <!--==============================Panel de reportes=========================-->
                <div role="tabpanel" class="tab-pane fade" id="Reportes">
                    <div class="row">
                      <div   class="col-xs-12 col-sm-offset-3 col-sm-6 ">
                          <br><br>

                              <h2 class="text-info text-center"><small><i class="fa fa-folder"></i></small>&nbsp;&nbsp;Reportes</h2>
                                    <input class="" type="date" id="bd-desde"/>
                                      <input class="" type="date" id="bd-hasta"/>
                                      <a target="_blank" href="javascript:reportePDF();" class="btn btn-danger">Exportar Busqueda a PDF</a>

                                      <br><div  class="registros" id="agrega-registros"></div>
                                        <input type="hidden" name="adm" id="adm" value="<?php echo $_SESSION['nombreAdmin']?>">
                          </div>
                      </div>

                    </div>
                <!--==============================Panel pedidos===============================-->
                <div role="tabpanel" class="tab-pane fade" id="Pedidos">
                    <div class="row">
                        <div class="col-xs-12">
                            <br><br>
                            <div id="del-pedido">
                                <h2 class="text-danger text-center"><small><i class="glyphicon glyphicon-trash"></i></small>&nbsp;&nbsp;Eliminar pedido</h2>
                                <form action="process/delPedido.php" method="post" role="form">
                                    <div class="form-group">
                                        <label>Pedidos</label>
                                        <select class="form-control" name="num-pedido">
                                          <?php $nadmin=$_SESSION['nombreAdmin'];
                                                $pedidoC=  ejecutarSQL::consultar("select * from venta where NombreA='$nadmin'");
                                                while($pedidoD=mysqli_fetch_array($pedidoC)){
                                                    echo '<option value="'.$pedidoD['NumPedido'].'">Pedido #'.$pedidoD['NumPedido'].' - Estado('.$pedidoD['Estado'].') - Fecha('.$pedidoD['Fecha'].')</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <p class="text-center"><button type="submit" class="btn btn-danger">Eliminar pedido</button></p>
                                    <br>
                                    <div id="res-form-del-pedido" style="width: 100%; text-align: center; margin: 0;"></div>
                                </form>
                            </div>
                            <br><br>
                             <div class="panel panel-success">
                               <div class="panel-heading text-center"><h3>Actualizar estado de pedido</h3></div>
                              <div class="table-responsive">
                                  <table class="table table-bordered">
                                      <thead class="">
                                          <tr>
                                              <th class="text-center">#</th>
                                              <th class="text-center">Fecha</th>
                                              <th class="text-center">Cliente</th>
                                              <th class="text-center">Total</th>
                                              <th class="text-center">Estado</th>
                                              <th class="text-center">Opciones</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                            <?php
                                            $nadmin=$_SESSION['nombreAdmin'];
                                            $pedidoU=  ejecutarSQL::consultar("select * from venta where NombreA='$nadmin'");
                                            $upp=1;
                                            while($peU=mysqli_fetch_array($pedidoU)){
                                                echo '
                                                    <div id="update-pedido">
                                                      <form method="post" action="process/updatePedido.php" id="res-update-pedido-'.$upp.'">
                                                        <tr>
                                                            <td>'.$peU['NumPedido'].'<input type="hidden" name="num-pedido" value="'.$peU['NumPedido'].'"></td>
                                                            <td>'.$peU['Fecha'].'</td>
                                                            <td>';
                                                                $conUs= ejecutarSQL::consultar("select * from cliente where Nombre='".$peU['Nombre']."'");
                                                                while($UsP=mysqli_fetch_array($conUs)){
                                                                    echo $UsP['Nombre'];
                                                                }
                                                    echo   '</td>
                                                            <td>'.$peU['TotalPagar'].'</td>
                                                            <td>
                                                                <select class="form-control" name="pedido-status">';
                                                                    if($peU['Estado']=="Pendiente"){
                                                                       echo '<option value="Pendiente">Pendiente</option>';
                                                                       echo '<option value="Entregado">Entregado</option>';
                                                                    }
                                                                    if($peU['Estado']=="Entregado"){
                                                                       echo '<option value="Entregado">Entregado</option>';
                                                                       echo '<option value="Pendiente">Pendiente</option>';
                                                                    }
                                                    echo        '</select>
                                                            </td>
                                                            <td class="text-center">
                                                                <button type="submit" class="btn btn-sm btn-primary button-UPPE" value="res-update-pedido-'.$upp.'">Actualizar</button>
                                                                ';?>
                                                                <href data-toggle="modal" class="btn btn-sm btn-success" data-target="#pedi-<?php echo $peU['NumPedido'];?>">Detalles</href>
                                                                <div class="modal fade" id="pedi-<?php echo $peU['NumPedido'];?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $peU['NumPedido'];?>-Label" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                      <div class="modal-content">

                                                                        <div class="modal-header">
                                                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                          <h4 class="modal-title"  style="text-align:center;"  id="<?php echo $peU['NumPedido'];?>-Label">Detalles del pedido</h4>
                                                                        </div>

                                                                          <div class="table-responsive">
                                                                              <table class="table table-bordered">
                                                                                <thead>
                                                                                  <tr>
                                                                                    <td>Codigo de Producto</td>
                                                                                    <td>Nombre del Producto</td>
                                                                                    <td>Cantidad de Productos</td>
                                                                                    <td>Imagen</td>
                                                                                  </tr>
                                                                                </thead>
                                                                        <?php $pediconsu= ejecutarSQL::consultar("select * from detalle where NumPedido='". $peU['NumPedido']."'");
                                                                        while($Us=mysqli_fetch_array($pediconsu)){
                                                                          $pconsu= ejecutarSQL::consultar("select * from producto where CodigoProd='".$Us['CodigoProd']."'");
                                                                          while($Upp=mysqli_fetch_array($pconsu)){?>
                                                                                <tbody>
                                                                                  <tr>
                                                                              <td><?php echo $Us['CodigoProd'];?></td>
                                                                              <td><?php echo $Upp['NombreProd'];?></td>
                                                                              <td><?php echo $Us['CantidadProductos'];?></td>
                                                                            <td>
                                                                              <img width="50px" height="50px" src="assets/img-products/<?php echo $Upp['Imagen'] ?>">
                                                                           </td></tr>
                                                                                </tbody>
                                                                             <?php }
                                                                        }
                                                                        ?></table>
                                                                      </div>

                                                                        <div class="modal-footer">
                                                                          <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
                                                                        </div>
                                                                   </div>
                                                                    </div>
                                                                  </div>

                                                                <?php echo '
                                                                <div id="res-update-pedido-'.$upp.'" style="width: 100%; margin:0px; padding:0px;"></div>
                                                            </td>
                                                        </tr>
                                                      </form>
                                                    </div>
                                                    ';
                                                $upp=$upp+1;
                                            }
                                          ?>
                                      </tbody>
                                  </table>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include './inc/footer.php'; ?>
    <script>
    $(document).ready(function(){
      load(1);
    });

    function load(page){
      var nadmin=document.getElementById('name-admin').value;
      var parametros = {"action":"ajax","page":page,"nadmin":nadmin};
      $("#loader").fadeIn('slow');
      $.ajax({
        url:'administracion_ajax.php',
        type: "POST",
        data: parametros,
        success:function(data){
          $(".outer_div").html(data).fadeIn('slow');
          $("#loader").html("");
        }
      })
    }
    </script>
    <script>
    $("#actulizarcli").on("submit", function(e){
      e.preventDefault();
      var formData = new FormData(document.getElementById("actulizarcli"));
      $.ajax({
        url: "process/auctclien.php",
        type: "POST",
        dataType: "HTML",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
      }).done(function(echo){
        $("#mensaje2").html(echo);
      });
    });
    </script>
    <script>
    $("#actulizarcontra").on("submit", function(e){
      e.preventDefault();
      var formData = new FormData(document.getElementById("actulizarcontra"));
      $.ajax({
        url: "process/auctcontra.php",
        type: "POST",
        dataType: "HTML",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
      }).done(function(echo){
        $("#mensaje1").html(echo);
      });
    });
    </script>
    <script>
      function pagina1(page){
        var nadmin=document.getElementById('name-admin').value;
        var consu=document.getElementById('opcconsulta').value;
        var parametros = {"action":"ajax","page":page,"consu":consu,"nadmin":nadmin};
        $("#loader").fadeIn('slow');
        $.ajax({
          url:'opcconsulta1.php',
          type:"POST",
          data: parametros,
          success:function(data){
            $(".outer_div").html(data).fadeIn('slow');
            $("#loader").html("");
          }
        });
        return false;
      }
    </script>
    <script>
      function pagina(page){
        var nadmin=document.getElementById('name-admin').value;
        var consu=document.getElementById('consu').value;
        var parametros = {"action":"ajax","page":page,"consu":consu,"nadmin":nadmin};
        $("#loader").fadeIn('slow');
        $.ajax({
          url:'consulta1.php',
          type:"POST",
          data: parametros,
          success:function(data){
            $(".outer_div").html(data).fadeIn('slow');
            $("#loader").html("");
          }
        });
        return false;
      }
    </script>
    <script>
    $('#bd-desde').on('change', function(){
      var desde = $('#bd-desde').val();
      var adm = $('#adm').val();
      var hasta = $('#bd-hasta').val();
      var url = './php/busca_producto_fecha.php';
      $.ajax({
      type:'POST',
      url:url,
      data:'desde='+desde+'&hasta='+hasta+'&adm='+adm,
      success: function(datos){
        $('#agrega-registros').html(datos);
      }
    });
    return false;
    });

    $('#bd-hasta').on('change', function(){
      var desde = $('#bd-desde').val();
      var adm = $('#adm').val();
      var hasta = $('#bd-hasta').val();
      var url = './php/busca_producto_fecha.php';
      $.ajax({
      type:'POST',
      url:url,
      data:'desde='+desde+'&hasta='+hasta+'&adm='+adm,
      success: function(datos){
        $('#agrega-registros').html(datos);
      }
    });
    return false;
    });
    function reportePDF(){
    	var desde = $('#bd-desde').val();
    	var hasta = $('#bd-hasta').val();
      var adm = $('#adm').val();
    	window.open('./php/productos.php?desde='+desde+'&hasta='+hasta+'&adm='+adm);
    }
    </script>

</body>
</html>
