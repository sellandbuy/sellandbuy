<script src="js/carrito2.js"></script>
<?php
include 'library/configServer.php';
include 'library/consulSQL.php';
$nadmin=$_POST['nadmin'];
	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	if($action == 'ajax'){
		include 'pagination.php'; //incluir el archivo de paginación
		//las variables de paginación
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 8; //la cantidad de registros que desea mostrar
		$adjacents  = 4; //brecha entre páginas después de varios adyacentes
		$offset = ($page - 1) * $per_page;
		//Cuenta el número total de filas de la tabla*/

		$count_query   =ejecutarSQL::consultar("SELECT count(*) AS numrows FROM producto where Nombre='$nadmin' ");
		if ($row= mysqli_fetch_array($count_query)){$numrows = $row['numrows'];}
		$total_pages = ceil($numrows/$per_page);
		$reload = 'index.php';
		//consulta principal para recuperar los datos
		$query =ejecutarSQL::consultar("SELECT * FROM producto where Nombre='$nadmin' order by NombreProd LIMIT $offset,$per_page");

		if ($numrows>0){
			while($row = mysqli_fetch_array($query)){

                   echo '
                  <div class="col-xs-12 col-sm-6 col-md-3">
                       <div class="thumbnail">
                       <div id="imagee">
                         <img width="150px" height="150px" src="assets/img-products/'.$row['Imagen'].'">
                        </div>
                         <div class="caption text-center">
                           <h3>'.$row['Marca'].'</h3>
                           <p>'.$row['NombreProd'].'</p>
                           <p>$'.$row['Precio'].'</p>
                           <p class="text-center">
													 <a href="infoProd.php?CodigoProd='.$row['CodigoProd'].'" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-info-sign"></i>&nbsp; Detalles</a>&nbsp;&nbsp;';?>
		<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#edit-<?php echo $row['CodigoProd'] ?>"><span class="glyphicon glyphicon-edit"></span> Editar</button>
		<div class="modal fade" id="edit-<?php echo $row['CodigoProd'] ?>" tabindex="-1" role="dialog" aria-labelledby="editlLabel-<?php echo $row['CodigoProd'] ?>">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="editLabel-<?php echo $row['CodigoProd'] ?>">Editar Producto</h4>
					</div>

					<form action="process/updateProduct.php" method="POST"  enctype="multipart/form-data">
					<div class="modal-body">
<?php echo'

							<input class="form-control" type="hidden" name="code-old-prod" required="" value="'.$row['CodigoProd'].'">
							<label>Codigo de Producto</label><input class="form-control" disabled="true" type="text" name="code-prod" maxlength="30" required="" value="'.$row['CodigoProd'].'">

						<label>Nombre del Producto</label><input class="form-control" type="text" name="prod-name" maxlength="30" required="" value="'.$row['NombreProd'].'">
<label>Categoria</label>
						';

						$categoriac3=  ejecutarSQL::consultar("select * from categoria where CodigoCat='".$row['CodigoCat']."'");
						while($catec3=mysqli_fetch_array($categoriac3)){
								$codeCat=$catec3['CodigoCat'];
								$nameCat=$catec3['Nombre'];
						}
						echo '<select class="form-control" name="prod-category">';
								echo '<option value="'.$codeCat.'">'.$nameCat.'</option>';
								$categoriac2=  ejecutarSQL::consultar("select * from categoria");
								while($catec2=mysqli_fetch_array($categoriac2)){
										if($catec2['CodigoCat']==$codeCat){

										}else{
											echo '<option value="'.$catec2['CodigoCat'].'">'.$catec2['Nombre'].'</option>';
										}

								}
						echo '</select>

						<label>Precio</label><input class="form-control" type="text-area" name="price-prod" required="" value="'.$row['Precio'].'">
						<label>Modelo</label><input class="form-control" type="tel" name="model-prod" required="" maxlength="20" value="'.$row['Modelo'].'">
						<label>Marca</label><input class="form-control" type="text-area" name="marc-prod" maxlength="30" required="" value="'.$row['Marca'].'">
						<label>Stock</label><input class="form-control" type="text-area" name="stock-prod" maxlength="30" required="" value="'.$row['Stock'].'">
						';

					 ?>

					</div>
					<div class="modal-footer">
						<button type="button"  class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						<button type="submit"  class="btn btn-primary" >Actualizar</button>
					</div>
				</form>

				</div>

			</div>
		</div>


			 <button data-toggle="modal" data-target="#delete-<?php echo $row['CodigoProd'] ?>" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
				<div class="modal fade" id="delete-<?php echo $row['CodigoProd'] ?>" tabindex="-1" role="dialog" aria-labelledby="deleteLabel-<?php echo $row['CodigoProd'] ?>" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">

								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" id="deleteLabel-<?php echo $row['CodigoProd'] ?>">Eliminar Producto</h4>
								</div>

								<form id="del-prod-form" action="process/delprod.php" method="post" >
								<div class="modal-body">
									¿Desea eliminar este producto?
													<input type="hidden" name="prod-code" value="<?php echo $row['CodigoProd'] ?>">

								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
									<button type="submit"  class="btn btn-danger">Eliminar</button>
								</div>
							</form>
							<script>
							$('#del-prod-form').submit(function(e) {
											 e.preventDefault();

											 var informacion=$('#del-prod-form').serialize();
											 var metodo=$('#del-prod-form').attr('method');
											 var peticion=$('#del-prod-form').attr('action');
											 $.ajax({
													type: metodo,
													url: peticion,
													data:informacion,
													beforeSend: function(){
															$("#res-form-del-prod").html('Eliminando producto class="center-all-contens">');
													},
													error: function() {
															$("#res-form-del-prod").html("Ha ocurrido un error en el sistema");
													},
													success: function (data) {
															$("#res-form-del-prod").html(data);
													}
											});
											return false;
									});
							</script>

							</div>
						</div>
					</div>
												</p>
                         </div>
                       </div>
                   </div>

<?php
			}
			?>
		<div class="col-xs-12 col-sm-12 col-md-12 table-pagination ">
			<?php echo paginate($reload, $page, $total_pages, $adjacents);?>
		</div>

			<?php

		} else {
			?>
			<div class="alert alert-warning alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4>Aviso!!!</h4> No hay datos para mostrar
            </div>
			<?php
		}
	}
?>
